var maxNoofPlaces = 5;
$(document).ready(function(){
	//Bind click event on Search
	  $("#getIt").bind("click", function(){
		var placeName = $("#search").val().trim();
		var patt = new RegExp("^[a-zA-Z]+[a-zA-Z, ]*$");
		if(!patt.test(placeName)) {
		  displayError("Please input a valid place name.");
                }
		else {
			//Send JSON request
			  var requestUrl = "https://api.foursquare.com/v2/venues/search";
			  var requestData = "oauth_token=CLNVAE2TSVZKY5MJO0JGPCBASPYVSHRTZWSNSSGOYKSSE13K&v=20150904&limit="+maxNoofPlaces+"&intent=browse&radius=1000&near="+placeName;
			  $.getJSON(requestUrl,requestData).done(function(jsonData){
					displayVenues(jsonData);
				}).fail(function(jqxhr, textStatus, error ){
					var jsonErr = $.parseJSON(jqxhr.responseText);
					displayError(jsonErr.meta.errorDetail);
			  });
		}
		
	  });
});
	
// Display the error
function displayError(errorMsg){
	//Hide the venues details and map
	$("#response .response-heading").addClass("hide");
	$("#response .response-heading .input-place_address").html('');
	$("#place-listing").addClass("hide").html('');
	$("#map").addClass("hide");
	$("#response .error").html(errorMsg).removeClass("hide");
}
//Display all the venues with their addresses 
function displayVenues(jsonData){
     //Hide the errors, if any.
     $("#response .error").addClass("hide").html(''); 	
     //Display heading	
     $("#response .response-heading span.input-place_address").html(jsonData.response.geocode.feature.highlightedName);
     //Creating array of all venues to show in google maps
     var map_locations = [];
     //variables for central latitude, longitude in google maps
     var centre_lat = 0, centre_lng = 0;
     //Create a list of all the venues
     var innerHTML ='<ul>';
     //Iterate over the venues
     $.each(jsonData.response.venues, function(i, venue) {
         
         innerHTML += '<li>';
         innerHTML += '<div id="venue-'+i+'">';
         innerHTML += '<span class="venue_name">'+venue.name+'</span><br/>';
         innerHTML += '<span id="address">Address : ';
         if(venue.location.formattedAddress != undefined){
           var length = venue.location.formattedAddress.length;
           $.each(venue.location.formattedAddress,function(j,address){
             innerHTML += address;
             if (j!=length-1){
               innerHTML += ", ";
             }
           });
         }
          innerHTML += '</span><br/>';
          innerHTML += '<span id="category">Category : ';
          if(venue.categories != undefined){
            var length = venue.categories.length;
            $.each(venue.categories,function(j,category){
              innerHTML += category.pluralName;
             if (j!=length-1){
               innerHTML += ", ";
             }
          });
         }
         innerHTML += '</span><br/>';
         innerHTML += '</div><br/>';
         innerHTML += '</li>';
		 var map_loc = [venue.name,venue.location.lat, venue.location.lng, i+1];
         map_locations.push(map_loc);
         centre_lat +=venue.location.lat;
         centre_lng +=venue.location.lng;
      });
    innerHTML+='</ul>';
    $("#place-listing").html(innerHTML);
    $("#response .response-heading").removeClass("hide");
    $("#place-listing").removeClass("hide");
    $("#map").removeClass("hide");
    displayMap(map_locations, centre_lat/maxNoofPlaces, centre_lng/maxNoofPlaces);
}
function displayMap(locations, centre_lat, centre_lng){
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 13,
      center: new google.maps.LatLng(centre_lat, centre_lng),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var infowindow = new google.maps.InfoWindow();
    var marker, i;
    for (i = 0; i < locations.length; i++) {
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
}