--The project has been developed using HTML, JAVASCRIPT and CSS.
--The page (PlaceSearch/findPlace.html) contains a textbox to 
  input the place to search and click on Search.
--It displays the top 5 venues returned with their addresses
  and categories.
--An AJAX request is fired with the required parameters and 
  then the JSON response is used to show the results.
--In case of error or invalid input, error message is shown.
--A valid input is a string containing alphabets, spaces and 
  comma. Example : Piccadilly, UK

--GoogleMaps shows the venues on map.
--On clicking on marker pin, name of the venue is displayed.
--The latitude and longitudes of the venues are sent to google 
  maps to display the marker pins.